package dev.kotylu.auth.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DemoController {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public String hello() {
        return "Hello world!";
    }

    @RequestMapping(value = "/noauth", method = RequestMethod.GET)
    @ResponseBody
    public String noauth() {
        return "Hello anony: world!";
    }
}
