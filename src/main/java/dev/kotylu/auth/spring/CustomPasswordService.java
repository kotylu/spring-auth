package dev.kotylu.auth.spring;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UserDetailsService;

public class CustomPasswordService implements UserDetailsPasswordService {
    private DaoAppUsers appUsers;
    private UserDetailsService userService;

    public CustomPasswordService() {
        appUsers = DaoAppUsers.getInstance();
        userService = new CustomUserDetailsService();
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        appUsers.setPasswordHash(user.getUsername(), newPassword);
        return userService.loadUserByUsername(user.getUsername());
    }
}
