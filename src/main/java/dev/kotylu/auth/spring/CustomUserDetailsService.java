package dev.kotylu.auth.spring;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomUserDetailsService implements UserDetailsService {
    private DaoAppUsers users;
    public CustomUserDetailsService() {
        users = DaoAppUsers.getInstance();
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails user = User.withUsername(username).password(users.getPasswordHash(username)).build();
        return user;
    }
}
