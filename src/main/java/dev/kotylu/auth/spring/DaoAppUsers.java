package dev.kotylu.auth.spring;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaoAppUsers {
    public static DaoAppUsers getInstance() {
        if (instance == null) {
            return new DaoAppUsers();
        }
        return instance;
    }
    private static DaoAppUsers instance;
    private Map<String, String> db;

    private DaoAppUsers() {
        db = new HashMap<>();
        db.put("kotylu", "Ab123");
    }

    public String getPasswordHash(String username) {
        return db.get(username);
    }

    public void setPasswordHash(String username, String hash) {
        db.put(username, hash);
    }

    public void addUser(String name, String passHash) {
        db.put(name, passHash);
    }
}
